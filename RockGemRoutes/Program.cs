﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RockGemRoutes
{


    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmRockGemRoutes_main());
        }

        static public List<Route> routes = new List<Route>();

        // Adds the passed route to the routes list and returns the index where added.
        static public int addRoute(Route route)
        {
            if (routes == null) routes = new List<Route>();
            routes.Add(route);
            return routes.Count - 1;
        }


    }

   
}
