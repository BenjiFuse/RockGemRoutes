﻿using System;

namespace RockGemRoutes
{
    public class Route
    {
        public string title;
        public string color;
        public string location;
        public string difficulty;
        public string description;
        public string setter;
        public DateTime setDate;
        public DateTime cleanDate;

        public Route(string title, string color, string location, string difficulty, string description, string setter, DateTime setDate)
        {
            this.title = title;
            this.color = color;
            this.location = location;
            this.difficulty = difficulty;
            this.description = description;
            this.setter = setter;
            this.setDate = setDate;
            this.cleanDate = DateTime.MaxValue;
        }

        // overloads ToString to return route object fields
        public override string ToString()
        {
            string r = "title: " + title;
            r += "\ncolor: " + this.color;
            r += "\nlocation: " + this.location;
            r += "\ndifficulty: " + this.difficulty;
            r += "\ndescription: " + this.description;
            r += "\nsetter: " + this.setter;
            r += "\nsetDate: " + this.setDate;
            r += "\ncleanDate: " + this.cleanDate;

            return r;
        }
    }

}