﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RockGemRoutes
{
    public partial class frmRockGemRoutes_error : Form
    {
        string title;
        string description;
        string option1, option2;
        string additionalInfo;

        // Default constructor to accept title, description, and button text
        public frmRockGemRoutes_error(string title, string description, string option1, string option2)
        {
            this.title = title;
            this.description = description;
            this.option1 = option1;
            this.option2 = option2;
            InitializeComponent();
        }

        // Additional constructor to also accept additional info
        public frmRockGemRoutes_error(string title, string description, string option1, string option2, string additionalInfo)
        {
            this.title = title;
            this.description = description;
            this.additionalInfo = additionalInfo;
            this.option1 = option1;
            this.option2 = option2;
            InitializeComponent();
        }

        private void btnOption2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOption1_Click(object sender, EventArgs e)
        {
          
            txtAdditionalInfo.Show();
            

        }

        private void frmRockGemRoutes_error_Resize(object sender, EventArgs e)
        {
            Control control = (Control)sender;

            pnlContent.Size = new Size(control.Size.Width, control.Size.Height / 2);
            
        }

        // Instantiate all form text
        private void Form1_Load(object sender, EventArgs e)
        {
            Text = title;
            lblDescription.Text = description;
            txtAdditionalInfo.Text = additionalInfo;
            btnOption1.Text = option1;
            btnOption2.Text = option2;
            txtAdditionalInfo.Hide();

            Size = new Size(Size.Width, Size.Height - 56);
        }
    }
}
