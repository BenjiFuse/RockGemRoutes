﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Newtonsoft.Json;

namespace RockGemRoutes
{
    public partial class frmRockGemRoutes_AddNew : Form
    {
        Form parent = null;

        // Default constructor accepts reference to parent form (for updating table on main)
        public frmRockGemRoutes_AddNew(Form parent)
        {
            InitializeComponent();
            this.parent = parent;
        }
        
        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtTitle.Text = null;
            this.btnColor.BackColor = SystemColors.Control;
            this.cboLocation.Text = null;
            this.cboDifficulty.Text = null;
            this.txtDescription.Text = null;
            this.cboSetter.Text = null;
            this.dtpRouteDate.Value = DateTime.Now;

        }

        private void btnColor_Click(object sender, EventArgs e)
        {
            ColorDialog routeColorPicker = new ColorDialog();

            if (routeColorPicker.ShowDialog() == DialogResult.OK)
            {
                btnColor.BackColor = routeColorPicker.Color;
            }

        }

        private void btnArchive_Click(object sender, EventArgs e)
        {
            // Ensure all fields are valid first
            if (txtTitle.Text == "")
            {
                MessageBox.Show(this, "Please enter a Title!");
            }
            else if (txtDescription.Text == "")
            {
                MessageBox.Show(this, "Please enter a description!");
            }
            else if (btnColor.BackColor == SystemColors.Control)
            {
                MessageBox.Show(this, "Please choose a tape color!");
            }
            else if (cboLocation.Text == "")
            {
                MessageBox.Show(this, "Please choose a location!");
            }
            else if (cboDifficulty.Text == "")
            {
                MessageBox.Show(this, "Please choose a difficulty rating!");
            }
            else if (cboSetter.Text == "")
            {
                MessageBox.Show(this, "Please choose a setter!");
            }
            else
            {
                Route newRoute = new Route(txtTitle.Text, btnColor.BackColor.ToString(), cboLocation.Text,
                    cboDifficulty.Text, txtDescription.Text, cboSetter.Text, dtpRouteDate.Value);

                Program.addRoute(newRoute);
                MessageBox.Show(this, "Route Archived Successfully.");
            }

            ((frmRockGemRoutes_main)parent).updateRoutesTable();
            this.Close();
        }

    }

}
