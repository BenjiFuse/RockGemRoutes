﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RockGemRoutes
{
    public partial class frmRockGemRoutes_routeMap : Form
    {
        Form parent = null;

        // Default constructor to keep parent form reference
        // Loads floor map image and draws colored route points
        public frmRockGemRoutes_routeMap(Form parent)
        {
            InitializeComponent();
            this.parent = parent;

            OpenFileDialog openMapFileDialog = new OpenFileDialog();

            if (openMapFileDialog.ShowDialog() == DialogResult.OK)
            {
                string mapFile = openMapFileDialog.FileName;
                string mapPath = Path.GetFullPath(mapFile);
                picRouteMap.Image = Image.FromFile(mapPath);
                //this.drawPoints();
            } else {
                new frmRockGemRoutes_error("Map File Error",
                    "ERROR:\nMap File invalid or not found.",
                    "Show Trace", "Return");
            }

            chart1.Series.First().ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
        }

        public void updateGraph()
        {
            chart1.Series.First().ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            
            
        }




        //private void RouteMap_Load()
        //{
        //    ElementHost host = new ElementHost();
        //    host.Dock = DockStyle.Fill;

        //    // Create the WPF UserControl.
        //    HostingWpfUserControlInWf.UserControl1 uc =
        //        new HostingWpfUserControlInWf.UserControl1();

        //    // Assign the WPF UserControl to the ElementHost control's
        //    // Child property.
        //    host.Child = uc;

        //    // Add the ElementHost control to the form's
        //    // collection of child controls.
        //    this.Controls.Add(host);

        //}

        //private drawPoints()
        //{
        //    this.picRouteMap
        //}


    }
}
