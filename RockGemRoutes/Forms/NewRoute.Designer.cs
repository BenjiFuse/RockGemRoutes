﻿namespace RockGemRoutes
{
    partial class frmRockGemRoutes_AddNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRockGemRoutes_AddNew));
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.btnColor = new System.Windows.Forms.Button();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.cboLocation = new System.Windows.Forms.ComboBox();
            this.lblDifficulty = new System.Windows.Forms.Label();
            this.cboDifficulty = new System.Windows.Forms.ComboBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.cboSetter = new System.Windows.Forms.ComboBox();
            this.lblSetter = new System.Windows.Forms.Label();
            this.dtpRouteDate = new System.Windows.Forms.DateTimePicker();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnArchive = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(12, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(27, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Title";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(45, 13);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(375, 20);
            this.txtTitle.TabIndex = 1;
            // 
            // btnColor
            // 
            this.btnColor.Location = new System.Drawing.Point(44, 37);
            this.btnColor.Name = "btnColor";
            this.btnColor.Size = new System.Drawing.Size(75, 23);
            this.btnColor.TabIndex = 2;
            this.btnColor.UseVisualStyleBackColor = true;
            this.btnColor.Click += new System.EventHandler(this.btnColor_Click);
            // 
            // lblColor
            // 
            this.lblColor.AutoSize = true;
            this.lblColor.Location = new System.Drawing.Point(11, 42);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(31, 13);
            this.lblColor.TabIndex = 4;
            this.lblColor.Text = "Color";
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(125, 42);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(48, 13);
            this.lblLocation.TabIndex = 5;
            this.lblLocation.Text = "Location";
            // 
            // cboLocation
            // 
            this.cboLocation.FormattingEnabled = true;
            this.cboLocation.Items.AddRange(new object[] {
            "Rope #1",
            "Rope #2",
            "Rope #3",
            "Rope #4",
            "Rope #5",
            "Rope #6",
            "Rope #7",
            "Rope #8",
            "Rope #9",
            "Rope #10"});
            this.cboLocation.Location = new System.Drawing.Point(179, 39);
            this.cboLocation.Name = "cboLocation";
            this.cboLocation.Size = new System.Drawing.Size(80, 21);
            this.cboLocation.TabIndex = 3;
            // 
            // lblDifficulty
            // 
            this.lblDifficulty.AutoSize = true;
            this.lblDifficulty.Location = new System.Drawing.Point(265, 42);
            this.lblDifficulty.Name = "lblDifficulty";
            this.lblDifficulty.Size = new System.Drawing.Size(47, 13);
            this.lblDifficulty.TabIndex = 7;
            this.lblDifficulty.Text = "Difficulty";
            // 
            // cboDifficulty
            // 
            this.cboDifficulty.FormattingEnabled = true;
            this.cboDifficulty.Items.AddRange(new object[] {
            "Novice",
            "Intermediate",
            "Advanced",
            "Expert"});
            this.cboDifficulty.Location = new System.Drawing.Point(318, 39);
            this.cboDifficulty.Name = "cboDifficulty";
            this.cboDifficulty.Size = new System.Drawing.Size(102, 21);
            this.cboDifficulty.TabIndex = 4;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(12, 71);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(15, 87);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(408, 62);
            this.txtDescription.TabIndex = 5;
            // 
            // cboSetter
            // 
            this.cboSetter.FormattingEnabled = true;
            this.cboSetter.Items.AddRange(new object[] {
            "LRM",
            "MAO",
            "AH",
            "JA",
            "EA",
            "BKF"});
            this.cboSetter.Location = new System.Drawing.Point(66, 155);
            this.cboSetter.Name = "cboSetter";
            this.cboSetter.Size = new System.Drawing.Size(93, 21);
            this.cboSetter.TabIndex = 6;
            // 
            // lblSetter
            // 
            this.lblSetter.AutoSize = true;
            this.lblSetter.Location = new System.Drawing.Point(12, 158);
            this.lblSetter.Name = "lblSetter";
            this.lblSetter.Size = new System.Drawing.Size(35, 13);
            this.lblSetter.TabIndex = 11;
            this.lblSetter.Text = "Setter";
            // 
            // dtpRouteDate
            // 
            this.dtpRouteDate.Location = new System.Drawing.Point(165, 156);
            this.dtpRouteDate.Name = "dtpRouteDate";
            this.dtpRouteDate.Size = new System.Drawing.Size(255, 20);
            this.dtpRouteDate.TabIndex = 7;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(12, 181);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 48);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnArchive
            // 
            this.btnArchive.Location = new System.Drawing.Point(332, 181);
            this.btnArchive.Name = "btnArchive";
            this.btnArchive.Size = new System.Drawing.Size(88, 48);
            this.btnArchive.TabIndex = 9;
            this.btnArchive.Text = "Archive";
            this.btnArchive.UseVisualStyleBackColor = true;
            this.btnArchive.Click += new System.EventHandler(this.btnArchive_Click);
            // 
            // frmRockGemRoutes_AddNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 236);
            this.Controls.Add(this.btnArchive);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.dtpRouteDate);
            this.Controls.Add(this.cboSetter);
            this.Controls.Add(this.lblSetter);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.cboDifficulty);
            this.Controls.Add(this.lblDifficulty);
            this.Controls.Add(this.cboLocation);
            this.Controls.Add(this.lblLocation);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.btnColor);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.lblTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRockGemRoutes_AddNew";
            this.Text = "Rock Gem Routes - New Route";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Button btnColor;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.ComboBox cboLocation;
        private System.Windows.Forms.Label lblDifficulty;
        private System.Windows.Forms.ComboBox cboDifficulty;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.ComboBox cboSetter;
        private System.Windows.Forms.Label lblSetter;
        private System.Windows.Forms.DateTimePicker dtpRouteDate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnArchive;
    }
}

