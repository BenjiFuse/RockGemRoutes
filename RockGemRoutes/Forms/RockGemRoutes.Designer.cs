﻿namespace RockGemRoutes
{
    partial class frmRockGemRoutes_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRockGemRoutes_main));
            this.btnAddNewRoute = new System.Windows.Forms.Button();
            this.menuRockGemRoutes = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.difficultyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpYourselfNerdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.routesDataGridView = new System.Windows.Forms.DataGridView();
            this.Title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tape = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Route_Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Difficulty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Setter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.setDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateCleaned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUpdateTable = new System.Windows.Forms.Button();
            this.btnGenerateTestData = new System.Windows.Forms.Button();
            this.lblCurrentArchive = new System.Windows.Forms.Label();
            this.viewsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.routeMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRockGemRoutes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.routesDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddNewRoute
            // 
            this.btnAddNewRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddNewRoute.AutoSize = true;
            this.btnAddNewRoute.Location = new System.Drawing.Point(12, 213);
            this.btnAddNewRoute.Name = "btnAddNewRoute";
            this.btnAddNewRoute.Size = new System.Drawing.Size(130, 36);
            this.btnAddNewRoute.TabIndex = 0;
            this.btnAddNewRoute.Text = "Add New Route";
            this.btnAddNewRoute.UseVisualStyleBackColor = true;
            this.btnAddNewRoute.Click += new System.EventHandler(this.btnAddNewRoute_Click);
            // 
            // menuRockGemRoutes
            // 
            this.menuRockGemRoutes.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.reportsToolStripMenuItem,
            this.viewsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuRockGemRoutes.Location = new System.Drawing.Point(0, 0);
            this.menuRockGemRoutes.Name = "menuRockGemRoutes";
            this.menuRockGemRoutes.Size = new System.Drawing.Size(771, 24);
            this.menuRockGemRoutes.TabIndex = 1;
            this.menuRockGemRoutes.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.manageToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "Open Archive";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.setArchiveLocationToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save as";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generateToolStripMenuItem,
            this.graphsToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // generateToolStripMenuItem
            // 
            this.generateToolStripMenuItem.Name = "generateToolStripMenuItem";
            this.generateToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.generateToolStripMenuItem.Text = "Generate";
            // 
            // graphsToolStripMenuItem
            // 
            this.graphsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.difficultyToolStripMenuItem});
            this.graphsToolStripMenuItem.Name = "graphsToolStripMenuItem";
            this.graphsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.graphsToolStripMenuItem.Text = "Graphs";
            // 
            // difficultyToolStripMenuItem
            // 
            this.difficultyToolStripMenuItem.Name = "difficultyToolStripMenuItem";
            this.difficultyToolStripMenuItem.Size = new System.Drawing.Size(122, 22);
            this.difficultyToolStripMenuItem.Text = "Difficulty";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpYourselfNerdToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpYourselfNerdToolStripMenuItem
            // 
            this.helpYourselfNerdToolStripMenuItem.Name = "helpYourselfNerdToolStripMenuItem";
            this.helpYourselfNerdToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.helpYourselfNerdToolStripMenuItem.Text = "Help Yourself Nerd";
            // 
            // routesDataGridView
            // 
            this.routesDataGridView.AllowUserToAddRows = false;
            this.routesDataGridView.AllowUserToDeleteRows = false;
            this.routesDataGridView.AllowUserToOrderColumns = true;
            this.routesDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.routesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.routesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Title,
            this.Tape,
            this.Route_Location,
            this.Difficulty,
            this.Setter,
            this.setDate,
            this.dateCleaned});
            this.routesDataGridView.Location = new System.Drawing.Point(12, 57);
            this.routesDataGridView.Name = "routesDataGridView";
            this.routesDataGridView.ReadOnly = true;
            this.routesDataGridView.ShowEditingIcon = false;
            this.routesDataGridView.Size = new System.Drawing.Size(747, 150);
            this.routesDataGridView.TabIndex = 3;
            // 
            // Title
            // 
            this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Title.HeaderText = "Title";
            this.Title.Name = "Title";
            this.Title.ReadOnly = true;
            this.Title.Width = 52;
            // 
            // Tape
            // 
            this.Tape.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Tape.HeaderText = "Tape";
            this.Tape.Name = "Tape";
            this.Tape.ReadOnly = true;
            this.Tape.Width = 57;
            // 
            // Location
            // 
            this.Route_Location.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Route_Location.HeaderText = "Location";
            this.Route_Location.Name = "Location";
            this.Route_Location.ReadOnly = true;
            this.Route_Location.Width = 73;
            // 
            // Difficulty
            // 
            this.Difficulty.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Difficulty.HeaderText = "Difficulty";
            this.Difficulty.Name = "Difficulty";
            this.Difficulty.ReadOnly = true;
            this.Difficulty.Width = 72;
            // 
            // Setter
            // 
            this.Setter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Setter.HeaderText = "Setter";
            this.Setter.Name = "Setter";
            this.Setter.ReadOnly = true;
            this.Setter.Width = 60;
            // 
            // setDate
            // 
            this.setDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.setDate.HeaderText = "Date Set";
            this.setDate.Name = "setDate";
            this.setDate.ReadOnly = true;
            this.setDate.Width = 74;
            // 
            // dateCleaned
            // 
            this.dateCleaned.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dateCleaned.HeaderText = "Date Cleaned";
            this.dateCleaned.Name = "dateCleaned";
            this.dateCleaned.ReadOnly = true;
            this.dateCleaned.Width = 97;
            // 
            // btnUpdateTable
            // 
            this.btnUpdateTable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdateTable.AutoSize = true;
            this.btnUpdateTable.Location = new System.Drawing.Point(629, 213);
            this.btnUpdateTable.Name = "btnUpdateTable";
            this.btnUpdateTable.Size = new System.Drawing.Size(130, 36);
            this.btnUpdateTable.TabIndex = 4;
            this.btnUpdateTable.Text = "Update Table";
            this.btnUpdateTable.UseVisualStyleBackColor = true;
            this.btnUpdateTable.Click += new System.EventHandler(this.btnUpdateTable_Click);
            // 
            // btnGenerateTestData
            // 
            this.btnGenerateTestData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGenerateTestData.AutoSize = true;
            this.btnGenerateTestData.Location = new System.Drawing.Point(490, 213);
            this.btnGenerateTestData.Name = "btnGenerateTestData";
            this.btnGenerateTestData.Size = new System.Drawing.Size(133, 36);
            this.btnGenerateTestData.TabIndex = 5;
            this.btnGenerateTestData.Text = "Generate Test Data";
            this.btnGenerateTestData.UseVisualStyleBackColor = true;
            this.btnGenerateTestData.Click += new System.EventHandler(this.btnGenerateTestData_Click);
            // 
            // lblCurrentArchive
            // 
            this.lblCurrentArchive.AutoSize = true;
            this.lblCurrentArchive.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentArchive.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCurrentArchive.Location = new System.Drawing.Point(12, 37);
            this.lblCurrentArchive.Name = "lblCurrentArchive";
            this.lblCurrentArchive.Size = new System.Drawing.Size(131, 17);
            this.lblCurrentArchive.TabIndex = 6;
            this.lblCurrentArchive.Text = "Current Archive: ";
            // 
            // viewsToolStripMenuItem
            // 
            this.viewsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.routeMapToolStripMenuItem});
            this.viewsToolStripMenuItem.Name = "viewsToolStripMenuItem";
            this.viewsToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.viewsToolStripMenuItem.Text = "Views";
            // 
            // routeMapToolStripMenuItem
            // 
            this.routeMapToolStripMenuItem.Name = "routeMapToolStripMenuItem";
            this.routeMapToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.routeMapToolStripMenuItem.Text = "Route Map";
            this.routeMapToolStripMenuItem.Click += new System.EventHandler(this.routeMapToolStripMenuItem_Click);
            // 
            // frmRockGemRoutes_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(771, 261);
            this.Controls.Add(this.lblCurrentArchive);
            this.Controls.Add(this.btnGenerateTestData);
            this.Controls.Add(this.btnUpdateTable);
            this.Controls.Add(this.routesDataGridView);
            this.Controls.Add(this.btnAddNewRoute);
            this.Controls.Add(this.menuRockGemRoutes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuRockGemRoutes;
            this.Name = "frmRockGemRoutes_main";
            this.Text = "RockGemRoutes";
            this.Load += new System.EventHandler(this.frmRockGemRoutes_main_Load);
            this.menuRockGemRoutes.ResumeLayout(false);
            this.menuRockGemRoutes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.routesDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddNewRoute;
        private System.Windows.Forms.MenuStrip menuRockGemRoutes;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpYourselfNerdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.DataGridView routesDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Route_Location;
        private System.Windows.Forms.Button btnUpdateTable;
        private System.Windows.Forms.Button btnGenerateTestData;
        private System.Windows.Forms.Label lblCurrentArchive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Title;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tape;
        private System.Windows.Forms.DataGridViewTextBoxColumn Difficulty;
        private System.Windows.Forms.DataGridViewTextBoxColumn Setter;
        private System.Windows.Forms.DataGridViewTextBoxColumn setDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateCleaned;
        private System.Windows.Forms.ToolStripMenuItem graphsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem difficultyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem routeMapToolStripMenuItem;
    }
}