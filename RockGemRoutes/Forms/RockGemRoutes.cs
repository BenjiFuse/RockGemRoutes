﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

using Newtonsoft.Json;


namespace RockGemRoutes
{
    public partial class frmRockGemRoutes_main : Form
    {
        string archivePath = null;  // String to hold open archive path
        string thousandWordsPath = null; // String to hold 1000 random word file for data generation
        string gemLayoutPath = null;    //
        Form addRouteForm = null;   // Reference to add-route child form
        Form routeMapForm = null;   // Ref to route-map form

        string directory = AppDomain.CurrentDomain.BaseDirectory;   // Get executing directory
        //C:\Users\Benja\Documents\Visual Studio 2015\Projects\RockGemRoutes\RockGemRoutes\Assets\1000RANDOMWORDS.txt


        public frmRockGemRoutes_main()
        {
            InitializeComponent();

            this.Text = directory;

        }

        // Updates Form DataViewGrid by re-adding all route rows.
        public void updateRoutesTable()
        {
            foreach (Route route in Program.routes)
            {
                this.routesDataGridView.Rows.Add(route.title, route.color.ToString(), route.location,
                    route.difficulty, route.setter, route.setDate, route.cleanDate);
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveArchiveAsFileDialog = new SaveFileDialog();
            saveArchiveAsFileDialog.Title = "Save File As";

            if (saveArchiveAsFileDialog.ShowDialog() == DialogResult.OK)
            {
                string archiveFile = saveArchiveAsFileDialog.FileName;
                archivePath = Path.GetFullPath(archiveFile);
                lblCurrentArchive.Text = "Current Archive:" + archivePath;
            }
            else
            {
                MessageBox.Show(this, "No file selected.");
                return; // early exit for file-open error
            }

            using (StreamWriter r = new StreamWriter(archivePath, false))   // overwrite file
            {
                r.Write(JsonConvert.SerializeObject(Program.routes));
            }
        }

        private void setArchiveLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openArchiveFileDialog = new OpenFileDialog();

            if (openArchiveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string archiveFile = openArchiveFileDialog.FileName;
                archivePath = Path.GetFullPath(archiveFile);
                lblCurrentArchive.Text = "Current Archive:" + archivePath;
            }
            else
            {
                MessageBox.Show(this, "No file selected.");
                return; // early exit for file-open error
            }

            using (StreamReader r = new StreamReader(archivePath))
            {
                try {
                    string json = r.ReadToEnd();
                    Program.routes = JsonConvert.DeserializeObject<List<Route>>(json);
                } catch (JsonReaderException ex)
                {
                    new frmRockGemRoutes_error("Archive Format Error",
                        "ERROR:\nArchive file corrupted. Content should be a JSON Serialized list of Route objects.",
                        "Show Trace", "Return", ex.StackTrace).Show();
                }
                
            }
        }

        private void btnAddNewRoute_Click(object sender, EventArgs e)
        {
            if (addRouteForm == null || addRouteForm.IsDisposed)
            {
                addRouteForm = new frmRockGemRoutes_AddNew(this);
            }

            addRouteForm.Show();
            
        }


        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (archivePath == null)
            {
                saveToolStripMenuItem.Enabled = false;
            } else
            {
                saveToolStripMenuItem.Enabled = true;
            }
        }

        private void btnUpdateTable_Click(object sender, EventArgs e)
        {
            updateRoutesTable();
        }

        private void btnGenerateTestData_Click(object sender, EventArgs e)
        {
            OpenFileDialog openWordsFileDialog = new OpenFileDialog();
            string wordsFile = null;
            string wordsPath = null;

            if (openWordsFileDialog.ShowDialog() == DialogResult.OK)
            {
                wordsFile = openWordsFileDialog.FileName;
                wordsPath = Path.GetFullPath(wordsFile);
            }
            else
            {
                new frmRockGemRoutes_error("Generartion Error",
                    "ERROR:\nWords File invalid or not found.",
                    "Show Trace", "Return");
            }

            Program.routes.Clear();

            Random rng = new Random();
            StreamReader r = new StreamReader(wordsPath);
            string text = r.ReadToEnd();
            string[] words = text.Split('\n');
            string[] colors = { "Red", "Orange", "Yellow", "Pink", "Blue", "Teal", "Purple", "Black", "White", "Owls", "Pickles" };
            string[] difficulties = { "Novice", "Intermediate", "Advanced", "Expert" };
            string[] setters = { "LRM", "MAO", "AH", "EA", "JA", "BKF" };

            // Generate 1000 randomized route entries
            for (int i = 0; i < 1000; i++)
            {
                string title = words[rng.Next(words.Length)] + " " + words[rng.Next(words.Length)];
                string color = colors[rng.Next(colors.Length)];
                string location = "Rope #" + (rng.Next(10) + 1);
                string difficulty = difficulties[rng.Next(difficulties.Length)];
                string setter = setters[rng.Next(setters.Length)];
                DateTime setDate = new DateTime(rng.Next(2000, 2016), rng.Next(1, 12), rng.Next(1, 28));

                Route route = new Route(title, color, location, difficulty, "", setter, setDate);
                Program.routes.Add(route);

            }

            updateRoutesTable();    // Update data grid view on form;
        }

        private void frmRockGemRoutes_main_Load(object sender, EventArgs e)
        {
            routesDataGridView.AutoResizeColumns();
        }

        private void routeMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (routeMapForm == null || routeMapForm.IsDisposed)
            {
                routeMapForm = new frmRockGemRoutes_routeMap(this);
            }

            routeMapForm.Show();

        }
    }
}
